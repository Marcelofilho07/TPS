# TPS
This is a ThirdPersonShooter Multiplayer Project. It took about 40hours to develop.
Create Session to Host a server. Join Session to find a server at random.
Use WASD to move, Left-Mouse-Button to shoot and Space bar to Jump.
There are power-up arround the map. Green balls recover life and Red balls double your damage.
Match Begins when all players click Ready.
Match Ends after 60seconds.

Developed with Unreal Engine 4
